function openPDF(url) {
	
  if (isLoading == false) {
		
   showSpinner();
   
   var requestData = {};
   requestData.username = username;
   requestData.token = APIKEY;
   requestData.password = password;
   requestData.domain = domain;
    
    isLoading = true;
      $.ajax({
          url: url,
          type: 'POST',
          data: requestData,
		  dataType: 'binary',		  
		  responseType:'arraybuffer',
		  processData: false,			
		  headers: {"token":"QWxhZGRpbjpvcGVuIHNlc2FtZQ"},
	      context: this,
          success: function (result) {
              isLoading = false;
			  try{resetXpull();}
			  catch(ex) {}
			  var arrayBufferView = new Uint8Array(result );
		      var blob = new Blob([arrayBufferView], {type: 'application/pdf'});
			  var fileURL = URL.createObjectURL(blob);
			  window.open(fileURL, '_blank', 'location=yes,enableViewportScale=yes');
			  hideSpinner();
              showDefaultSpinner = true;
          },
          error: function (xhr, ajaxOptions, thrownError) {
 	          isLoading = false;
              hideSpinner();
              showDefaultSpinner = true;
              try{resetXpull();}
			  catch(ex) {}
              showLoginPage();
              $("#lblErrorLogin").text(captions[currentLanguage].connection_error);
              $("#lblErrorLogin").show();
          }
      });

  } 
}



function submitForm(formID,URL,callBack) {

	var i = 0;
	var fd = new FormData();
    
	// form files
	if($('input[type="file"]')[0]) {
		var file_data = $('input[type="file"]')[0].files; // for multiple files
		for(i = 0;i<file_data.length;i++){
			fd.append("file_"+i, file_data[i]);
		}
	}
	
		
	//remaining form data
	form = $("#"+formID +" :input");
	
	for(var i=0; i < form.length; i++)
	{
		var e = form[i];
		if(e.type=='checkbox')
			fd.append(e.name,e.checked);	
		else
			fd.append(e.name,e.value);
	}	
	
    
   var fullURL = 'https://' + ip + ':' + port + '/' + URL;
   load(fullURL,callBack,null,fd); 
   
}



$.mobile.document
    // "filter-menu-menu" is the ID generated for the listview when it is created
    // by the custom selectmenu plugin. Upon creation of the listview widget we
    // want to prepend an input field to the list to be used for a filter.
    .on( "listviewcreate", "#filter-menu-menu", function( e ) {
        var input,
            listbox = $( "#filter-menu-listbox" ),
            form = listbox.jqmData( "filter-form" ),
            listview = $( e.target );
        // We store the generated form in a variable attached to the popup so we
        // avoid creating a second form/input field when the listview is
        // destroyed/rebuilt during a refresh.
        if ( !form ) {
            input = $( "<input data-type='search'></input>" );
            form = $( "<form></form>" ).append( input );
            input.textinput();
            $( "#filter-menu-listbox" )
                .prepend( form )
                .jqmData( "filter-form", form );
        }
        // Instantiate a filterable widget on the newly created listview and
        // indicate that the generated input is to be used for the filtering.
        listview.filterable({ input: input });
    })
    // The custom select list may show up as either a popup or a dialog,
    // depending how much vertical room there is on the screen. If it shows up
    // as a dialog, then the form containing the filter input field must be
    // transferred to the dialog so that the user can continue to use it for
    // filtering list items.
    //
    // After the dialog is closed, the form containing the filter input is
    // transferred back into the popup.
    .on( "pagebeforeshow pagehide", "#filter-menu-dialog", function( e ) {
        var form = $( "#filter-menu-listbox" ).jqmData( "filter-form" ),
            placeInDialog = ( e.type === "pagebeforeshow" ),
            destination = placeInDialog ? $( e.target ).find( ".ui-content" ) : $( "#filter-menu-listbox" );
        form
            .find( "input" )
            // Turn off the "inset" option when the filter input is inside a dialog
            // and turn it back on when it is placed back inside the popup, because
            // it looks better that way.
            .textinput( "option", "inset", !placeInDialog )
            .end()
            .prependTo( destination );
    });




function openPDF2(URL){
	try {
            window.plugins.ChildBrowser.showWebPage(URL,{ showLocationBar: true });
	}catch(err) 
	{
		alert(err.message);
	}
	
}
