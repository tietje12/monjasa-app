﻿var APIKEY = "64e7c706fd6f90342c090a7d13ac08a4";
var mainUrl ='';
var hashString = '';
var currentLanguage = 'dk';
var username = '';
var password = '';
var domain = '';
var ip = '';
var port = '';
var rememberMe = false;
var hwid = "";
var deviceType = 0;
var notificationCount = 0;
var loadingText = '';
var currentRoute = '';
var currentModuleRoute = '';
var moduleMode = false;
var applicationActive = false;
var isLoading = false;
var RefreshURL = '';
var systemRefresh = false;
var showDefaultSpinner = true;
var spinner;
var settings = {};
var captions = [];

function setupLanguages() {

	captions['gb'] = {};
	captions['dk'] = {};
	
	captions['gb'].remember_me = 'Remember me';
	captions['dk'].remember_me = 'Husk mig';
	
	captions['gb'].yes = 'Yes';
	captions['dk'].yes = 'Ja';	
	
	captions['gb'].no = 'No';
	captions['dk'].no = 'Nej';	
	
	captions['gb'].username = 'Username';
	captions['dk'].username = 'Brugernavn';	
	
	captions['gb'].password = 'Password';
	captions['dk'].password = 'Kodeord';	
	
	captions['gb'].login = 'Login';
	captions['dk'].login = 'Log på';	
	
	captions['gb'].login_error = 'Login error! please check login credentials';
	captions['dk'].login_error = 'Login fejl! Tjek venligst login informationer';	
	
	captions['gb'].connection_error = 'Connection error!';
	captions['dk'].connection_error = 'Forbindelses fejl!';

	captions['gb'].settings = 'Settings';
	captions['dk'].settings = 'Indstillinger';

	captions['gb'].refresh = 'Refresh';
	captions['dk'].refresh = 'Opdatér';

	captions['gb'].name = 'Name';
	captions['dk'].name = 'Navn';	
	
	captions['gb'].title = 'Title';
	captions['dk'].title = 'Stilling';	
	
	captions['gb'].thisdevice = 'This device';
	captions['dk'].thisdevice = 'Denne enhed';	
	
	captions['gb'].home = 'Home';
	captions['dk'].home = 'Hjem';

	captions['gb'].logout = 'Logout';
	captions['dk'].logout = 'Log ud';

	captions['gb'].email = 'Email';
	captions['dk'].email = 'Email';
	
	captions['gb'].loading = 'Loading';
	captions['dk'].loading = 'Indlæser';

	captions['gb'].currentLanguageName = 'ENG';	
	captions['dk'].currentLanguageName = 'DAN';
	
}

function isLoggedIn() {
    return ($.mobile.activePage.attr("id") == "index");
}
  
function changeLanguage(code) {

	currentLanguage = code;
	$("#txtUsername").attr("placeholder",captions[currentLanguage].username);
	$("#txtPassword").attr("placeholder",captions[currentLanguage].password);
	$("#btnLogin").text(captions[currentLanguage].login);
	$("#btnSettings").text(captions[currentLanguage].settings);
	$("#lblName").text(captions[currentLanguage].name);
	$("#lblTitle").text(captions[currentLanguage].title);
	$("#lblEmail").text(captions[currentLanguage].email);
	$("#lblLanguage").text(captions[currentLanguage].currentLanguageName);
    $("#lbl_remember_me").text(captions[currentLanguage].remember_me);
	
}

function resetSettings() {
		username = '';
		password = '';
		currentLanguage = 'gb';
		rememberMe = false;
		window.localStorage.setItem('username', username);
		window.localStorage.setItem('password', password); 
		window.localStorage.setItem('language', currentLanguage);
		window.localStorage.setItem('remember', rememberMe);
		loadSettings();
}

function getSetting(name)  {
    try {
        return window.localStorage.getItem(name);
    } catch (ex) {
        return '';
    }
}

function loadSettings() {

        currentLanguage = getSetting('language');
        if (!currentLanguage)
            resetSettings();

        $("#select_language").slider();
        $("#select_language").slider('refresh');



		rememberMe = getSetting('remember');
		if (rememberMe == "1") {
		    $("#select_remember")[0].selectedIndex = 1;
		    username = getSetting('username');
		    password = getSetting('password');
		} else
		    $("#select_remember")[0].selectedIndex = 0;
		
	
		$("#select_remember").slider();
		$("#select_remember").slider('refresh');
			
	
		domain = getSetting('domain');
		ip = 'navapps.monjasa.com';    // monjasa specifik
		port = '50280';                // monjasa specifik
		domain = 'Monjasa';            // monjasa specifik
		ip = '52.166.143.140';         // monjasa specifik
	
		$('#txtUsername').val(username);
		$('#txtPassword').val(password);


	}

function logOut()		{
		username = '';
		password = '';
		rememberMe = false;
		window.localStorage.setItem('username', username);
		window.localStorage.setItem('password', password); 
		window.localStorage.setItem('remember', rememberMe);
		
		$('#txtUsername').val(username);
		$('#txtPassword').val(password);
		
		$("#select_remember")[0].selectedIndex = 0;
		$("#select_remember").slider();
		$("#select_remember").slider('refresh');

		

}
	
function saveSettings()		{
    rememberMe = ($("#select_remember").val() == "yes");
	username =	$('#txtUsername').val();
	password = $('#txtPassword').val();
	if (rememberMe) {
	    window.localStorage.setItem('username', username);
	    window.localStorage.setItem('password', password);
	} else {
	    window.localStorage.setItem('username', '');
	    window.localStorage.setItem('password', '');
	}

	window.localStorage.setItem('language', currentLanguage);
	
	if (rememberMe)
	  window.localStorage.setItem('remember', "1");
    else
	  window.localStorage.setItem('remember', "0");
}

function createModuleList(res,callBack) {
	// userinfo
	var preloadURL = '';
	var preloadContainer = '';
	
    var userHTML = '<li>';
    var jd = {};
    jd.username = '';
    jd.title = '';
    jd.email = '';
    jd.modules = {};

	isLoading = true;
    try { jd = JSON.parse(res); } catch (ex) { }
	 
	  
    $('#left_panel ul').empty();

    if(!moduleMode) 
	  $('#main_content').empty();
	  
     //Opbyg bruger info header
      userHTML += '<div class="ui-corner-all custom-corners" >';
	  userHTML += '<div class="ui-bar ui-bar-a">';
	  userHTML += '<table style="padding: 0;margin: 0;">';
	  userHTML += '<tr><td>' + jd.username + '</td></tr>';
	  userHTML += '<tr><td>' + jd.title + '</td></tr>';
	  userHTML += '<tr><td>' + jd.email + '</td></tr>';
	  userHTML += '<table></div></div>';
	  userHTML +='</li>';
	  
	  
	  // tilføj moduler
	  $("#left_panel ul").append(userHTML);
	  $.each( jd.modules, function( key, module ) 
	  {
		 var container ='';
		 var content = '';
		 
		 if (module.location == "Swipe Menu" || module.location == "Both") 
	 	{
	    	    // 1, Create Swipe Menu Items
		    container = "#left_panel ul";
		    content = '<li id ="m_' + module.id + '"><a href="">' + module.description + '<span id ="c_' + module.id + '" class="ui-li-count">' + module.notifications.toString() + '</span></a></li>';
			$("#left_panel ul").append($(content).click(function() {
			    $("#left_panel").panel("close");
			    showMainPage(true);
				url = 'http://'+ip +':'+port+module.route;
				RefreshURL='#';
				moduleMode = true;
				currentModuleRoute = url;
				load(url, onModuleLoaded);
			}));

			if (module.notifications == 0)
			    $('#c_' + module.id).hide();

		 } 
 	 if (module.location == "Home Screen" || module.location == "Both") 
		{
            		if(!moduleMode) {
			    // 2. Create Main Items
				content = '<div id="m_'+module.id+'" data-role="collapsible" data-collapsed="'+module.collapsed+'" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right">' + 
							  '<h3>'+module.description+'</h3><p id="'+module.name+'_content"></p></div>';	
				container = "#main_content";
				if(module.collapsed=="false")  // så skal den preoades
				{
					preloadURL = 'http://'+ip +':'+port+module.route;
					preloadContainer =module.name+'_content';
				}
				//	hvis den ikke er collapsed, og dette ikke er en systemrefresh.. så skal den expandes.
				$(container).append($(content).collapsible({
				    expand: function (url) {
				        
						url = 'http://'+ip +':'+port+module.route;
						RefreshURL='#';
						moduleMode = false;
						currentModuleRoute = url;
						load(url,onHomeMenuExpand,module.name+'_content');
					}
				}));
            }
			}
	  });
	  $("#left_panel ul").append('<li id ="m_refresh"><a href="javascript:onRefreshClick()">' + captions[currentLanguage].refresh + '</a></li>');
	  $("#left_panel ul").append('<li id ="m_home"><a href="javascript:onHomeClick()">' + captions[currentLanguage].home + '</a></li>');
	  $("#left_panel ul").append('<li id ="m_device"><a href="javascript:thisDevice()">' + captions[currentLanguage].thisdevice + '</a></li>');
	  //$("#left_panel ul").append('<li id ="m_settings"><a href="javascript:showLoginPage()">' + captions[currentLanguage].settings + '</a></li>');
	  $("#left_panel ul").append('<li id ="m_logout"><a href="javascript:logOut();showLoginPage()">' + captions[currentLanguage].logout + '</a></li>');
	  $("#left_panel ul").listview('refresh');
	  $("#left_panel ul").trigger('create');

      // Create notificationlist
	  notificationCount = jd.notifications.length;
	  updateNotificationCount();

	  $('#right_panel ul').empty();
	  $.each(jd.notifications, function (key, notification) {
	      container = "#right_panel ul";
	      content = '<li id = "' + notification.module_id + '" guid="' + notification.guid + '" route="' + notification.route + '"><a href="">' + notification.message + '</a></li>';
	      $("#right_panel ul").append($(content).click(
              function (event) { onNotificationItemClick(notification,this); }
          ));
	  });

	  $("#right_panel ul").listview('refresh');
	  $("#right_panel ul").trigger('create');
	  $(document).on("swipeleft swiperight", "#right_panel li", function (event) {
	      event.stopImmediatePropagation();
	      onNotificationItemSwipe(event, this);
	  });

	  systemRefresh = false;

	  
	 isLoading = false; 
	 
	 if (callBack)
	     callBack();
	 else if(preloadURL!="")
		 load(preloadURL,onHomeMenuExpand,preloadContainer);
		 
  
}

function showMainPage(reset) {
    applicationActive = true;
    if (reset) {
        $("#main").show();
        $("#main_content").show();
        $("#loginContent").hide();
    }
    $.mobile.changePage("#index");    
}

function showLoginPage() {
    applicationActive = false;
    $("#lblErrorLogin").hide();
    $("#main").hide();
    $("#loginContent").show();
    $.mobile.changePage("#loginPage");
}

function pull_main() {
	if(isLoading == false) {
		showDefaultSpinner = false;
		refresh();
	}
}

function refresh() {
	
	if(isLoading == false)
	{
		url = 'http://' + ip + ':' + port + '/system/get_modules';
		if(moduleMode) {
			systemRefresh = true;
			load(currentModuleRoute, onModuleLoaded);
		}
		else
		 load(url, createModuleList);
	}
}

function loadCurrentModuleRoute() {
  load(currentModuleRoute, onModuleLoaded);
}

function loadModule() {
  load(currentModuleRoute, onModuleLoaded);
}

function onRefreshClick() {
    $("#left_panel").panel("close");
    $("#right_panel").panel("close");
    refresh();
}

function onHomeClick() {
    $("#left_panel").panel("close");
    $("#right_panel").panel("close");
    moduleMode = false;
    refresh();
}

function updateNotificationCount(module_id) {
    $('#lblNotifications').html(notificationCount.toString());
}

function onNotificationItemClick(notification,parent) {
    $("#right_panel").panel("close");
    var listitem = $(parent);
	if(notification.route!="") {
		url = 'http://'+ip +':'+port+notification.route;
		load(url, onModuleLoaded, listitem);
	};
}

function onNotificationItemSwipe(event,parent) {
	var listitem = $( parent ),
    dir = event.type === "swipeleft" ? "left" : "right",
    transition = $.support.cssTransform3d ? dir : false;
	removeNotification(listitem);
}

function removeNotification(listitem) {
    url = 'http://' + ip + ':' + port + '/system/remove_notification?guid=' + listitem.attr("guid");
    load(url, removeNotificationListItem, listitem);
}

function removeNotificationListItem(res,listitem) {
    listitem.remove();
    notificationCount--;
    updateNotificationCount(listitem[0].id);
    hideSpinner();
}

function onHomeMenuExpand(res,element){
	$("#"+element).html(res).trigger('create'); 
	$( "button" ).each(function( index ) {      
		$(this).click(customClickHandler);
	});
}

function onModuleLoaded(res, listItem) {

    moduleMode = true;

    if(RefreshURL=='#')
        currentModuleRoute = currentRoute;
    else if (RefreshURL == '')   {
        
    }
    else
        currentModuleRoute = RefreshURL;

    showMainPage(true);
    $("#main_content").empty();
    $("#main_content").html(res).trigger('create');   
	$( "button" ).each(function( index ) {       
		$(this).click(customClickHandler);
	});
				
    
	$(".nav_link").each(function (index) {      
	    $(this).click(customClickHandler);
	});

	if(listItem)
	    removeNotification(listItem);
	
	if (systemRefresh) {
	    url = 'http://' + ip + ':' + port + '/system/get_modules';
		showDefaultSpinner = false;
	    load(url, createModuleList);
    }
	
	
	    
}

function customClickHandler(e) {
    // Execute code in code tag
    var code;
    code = $(this).attr('execute');
    try {
        if (code)
            eval(code);
        } catch (ex)   {
      }
    
	RefreshURL = $(this).attr('RefreshURL');
	
    if ($(this).attr('value') != '#') 
	{
        url = 'http://' + ip + ':' + port + $(this).attr('value');
        if ($(this).attr('SystemRefresh')) {
             systemRefresh =true;
        }
        load(url, onModuleLoaded);
        e.preventDefault();
    }
    
}

function thisDevice() {
		
	url = 'http://' + ip + ':' + port + '/system/isalive?token=' + hwid + '&device_type=' + deviceType.toString();
    load(url,onDevice);
}

function onDevice() {
	$("#left_panel").panel("close");
    $("#right_panel").panel("close");
    
	alert("This device will now receive push messages");
}
function performLogin() {
    saveSettings();
	url = 'http://' + ip + ':' + port + '/system/isalive?token=' + hwid + '&device_type=' + deviceType.toString();
    load(url, onIsAlive);
}
  
function onIsAlive(res) {
    if (res.trim() == "1") {
        refresh();
    } else {
    showLoginPage();
    $("#lblErrorLogin").text(captions[currentLanguage].login_error);
    $("#lblErrorLogin").show();
    }
}

function showSpinner() {
	if (showDefaultSpinner) {
        $.mobile.loading("show", {
            textVisible: true,
            theme: "d",
            html: ""
        });
    }
}

function hideSpinner() {
	$.mobile.loading("hide");	
}

function load(url, callBack, passThroughData) {
	
  if (isLoading == false) {

	var xpull = $('#main').data("plugin_xpull");
	xpull.options.paused = true;
  
	loadSettings();
	currentRoute = url;
	showSpinner();
	
	//2017 Upgrade>>
    //var requestData = {};
    //requestData.username = username;
    //requestData.token = APIKEY;
    //requestData.password = password;
    //requestData.domain = domain;
    //requestData.language = captions[currentLanguage].currentLanguageName;
	requestData = new FormData();	  
	requestData.append("username",username);
	requestData.append("token",APIKEY);
	requestData.append("password",password);
	try {
		requestData.append("language",captions[currentLanguage].currentLanguageName);
	} catch(ex) 
	{
		requestData.append("language","DAN");
	}
	//2017 Upgrade<<

    isLoading = true;
      $.ajax({
          url: url,
          type: 'POST',
          data: requestData,
		  headers: {"token":"QWxhZGRpbjpvcGVuIHNlc2FtZQ"},
		  contentType: false,
		  processData: false,
	      context: this,
          success: function (result) {
              isLoading = false;
			  try{resetXpull();}
			  catch(ex) {}
              showMainPage(true);
              $.proxy(callBack, this)(result, passThroughData);
              hideSpinner();
              showDefaultSpinner = true;
			  xpull.options.paused = false;			  
          },
          error: function (xhr, ajaxOptions, thrownError) {
		         isLoading = false;
              hideSpinner();
              showDefaultSpinner = true;
              try{resetXpull();}
			  catch(ex) {}
              showLoginPage();
              $("#lblErrorLogin").text(captions[currentLanguage].connection_error);
              $("#lblErrorLogin").show();
			  xpull.options.paused = false;					  
          }
      });
  } 
}
	
function resetXpull() {
    $('#main').data('plugin_xpull').reset();
    }

// XOR Encryption/Descruption functions

function encrypt(s,pw) {
                       var a=0;
                         var resultString="";
                       var textLen=s.length;
                          	var pwLen=pw.length;
                          	for (i=0;i<textLen;i++) {
                             a=parseInt(s.charCodeAt(i));
                             a=a^(pw.charCodeAt(i%pwLen));
                             a=a+"";
                            while (a.length<3)
                              a="0"+a;
                            	resultString+=a;
                          }
                         return resultString;
                      }

function decrypt(s,pw)  { 
  var resultString=""; 
	var a=0;
	var pwLen=pw.length;
	var textLen=s.length;
	var i=0;
	var resultHolder="";
	while(i<s.length-2)
	{
		 resultHolder=s.charAt(i)+s.charAt(i+1)+s.charAt(i+2);
		 if (s.charAt(i)=="0")
		{
			resultHolder=s.charAt(i+1)+s.charAt(i+2);
		}
		 if ((s.charAt(i)=="0")&&(s.charAt(i+1)=="0"))
		 {
			resultHolder=s.charAt(i+2);
		 }
		 a=parseInt(resultHolder);
		 a=a^(pw.charCodeAt(i/3%pwLen));
		 resultString+=String.fromCharCode(a);
		 i+=3;
	} 
	return resultString;
  }





